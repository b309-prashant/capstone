"""
Problem: You have a social network represented as an undirected graph, where the nodes represent users and the edges represent friendships between users. You want to recommend new friends to a user based on the friends of their friends. Using graph theory and the breadth-first search algorithm, find the friends of a user's friends who are not already friends with the user and recommend them as new friends.

Solution:

We can use graph traversal and the breadth-first search algorithm to find the friends of a user's friends who are not already friends with the user. We can use the networkx module to implement this algorithm.

Environment Setup:
1. Create a virtual environment for the capstone project
> python -m venv <name-of-env>
2. Activate the environment
> source <name-of-env>/bin/activate (macOS)
3. Install Networkx and Matplotlib modules
> pip install networkx
> pip install matplotlib
4. Run the python script
> python3 -m capstone
"""

import networkx as nx

def recommend_friends(graph, user):
    user_friends = set(graph.neighbors(user))
    recommended_friends = set()
    visited = set([user])

    queue = []
    queue.extend(graph.neighbors(user))
    visited.update(queue)

    while queue:
        current_user = queue.pop(0)
        friends_of_friends = set(graph.neighbors(current_user))
        new_friends = friends_of_friends - visited
        recommended_friends.update(new_friends)
        queue.extend(new_friends)
        visited.update(new_friends)

    recommended_friends -= user_friends
    return recommended_friends


def load_graph(filename):
    graph = nx.Graph()
    with open(filename, 'r') as file:
        for line in file:
            user1, user2 = line.strip().split(',')
            graph.add_edge(user1, user2)
    return graph

graph = load_graph("friends.txt")


user = "John"
recommended = recommend_friends(graph, user)


print("Recommended friends for", user + ":")
for friend in recommended:
    print(friend)
